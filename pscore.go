package pscore

import (
	"fmt"
	"regexp"
)

/*
Password is an object used to calculate the score of the provided password.

Example:
p := pscore.NewPassword("Test1234")

if p.GetScore() < 80 {
	log.Fatal("Password score is less than 80.")
}
*/
type Password struct {
	score int
	word  string
	adds  map[string]metric
	deds  map[string]metric
}

//NewPassword creates a new instance of the Password type
func NewPassword(p string) *Password {
	newP := &Password{word: p}
	newP.calculate()
	return newP
}

//calculate runs the metrics against the defined password
func (p *Password) calculate() {
	//Inlined Additions
	l := len(p.word)
	p.score += l * 4

	p.adds = map[string]metric{
		"Lower Case":               &lowerCase{},
		"Middle Symbols & Numbers": &middleNumSymbols{},
		"Numbers":                  &numbers{},
		"Symbols":                  &symbols{},
		"Upper Case":               &upperCase{},
	}

	p.deds = map[string]metric{
		"Only Letters":           &onlyLetters{},
		"Only Numbers":           &onlyNumbers{},
		"Repeating Characters":   newRepeatCharacters(),
		"Consecutive Lower Case": &runningLower{},
		"Consecutive Numbers":    &runningNumbers{},
		"Consecutive Upper Case": &runningUpper{},
		"Sequential Characters":  &sequentialChars{},
	}

	//Streamed Metrics
	for i, c := range p.word {
		for _, m := range p.adds {
			m.consume(c, i, l)
		}

		for _, m := range p.deds {
			m.consume(c, i, l)
		}
	}
	for _, m := range p.adds {
		p.score += m.score(l)
	}

	for _, m := range p.deds {
		p.score -= m.score(l)
	}
}

//GetScore returns the calculated score of the password
func (p *Password) GetScore() int {
	return p.score
}

//GetScoreDetails returns a detailed report of how the passwords
//score was calculated.
func (p *Password) GetScoreDetails() string {
	l := len(p.word)
	output := fmt.Sprintln("Additions:")
	fmt.Sprintln("Length:", l*4)
	for n, m := range p.adds {
		output += fmt.Sprintln(n+":", m.score(l))
	}

	output += fmt.Sprintln("\nDeductions:")
	for n, m := range p.deds {
		output += fmt.Sprintln(n+":", m.score(l))
	}
	output += fmt.Sprintln("\nScore:", p.score)
	return output
}

//metric interface is used to add or deduct points from a password
type metric interface {
	consume(rune, int, int)
	score(int) int
}

var (
	digitRgx, _  = regexp.Compile("\\d")
	lowerRgx, _  = regexp.Compile("[a-z]")
	upperRgx, _  = regexp.Compile("[A-Z]")
	symbolRgx, _ = regexp.Compile("\\W")
)

//isDigit determines if the provided rune is a digit
func isDigit(c rune) bool {
	return digitRgx.MatchString(string(c))
}

//isLowerCase determines if the provided rune is a lower case letter
func isLowerCase(c rune) bool {
	return lowerRgx.MatchString(string(c))
}

//isUpperCase determines if the provided rune is an upper case letter
func isUpperCase(c rune) bool {
	return upperRgx.MatchString(string(c))
}

//isSymbol determines if the provided rune is a symbol
func isSymbol(c rune) bool {
	return symbolRgx.MatchString(string(c))
}
