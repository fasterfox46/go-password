package pscore

/*
onlyLetters deducts a point for every character in a password
if it consists solely of letters
*/
type onlyLetters struct {
	invalid bool
}

func (ol *onlyLetters) consume(c rune, i, l int) {
	if !ol.invalid && !isLowerCase(c) && !isUpperCase(c) {
		ol.invalid = true
	}
}

func (ol *onlyLetters) score(l int) int {
	if ol.invalid {
		return 0
	}
	return l
}

/*
onlyNumbers deducts a point for every character in a password
if it consists solely of numbers
*/
type onlyNumbers struct {
	invalid bool
}

func (on *onlyNumbers) consume(c rune, i, l int) {
	if !on.invalid && !isDigit(c) {
		on.invalid = true
	}
}

func (on *onlyNumbers) score(l int) int {
	if on.invalid {
		return 0
	}
	return l
}

/*
repeatCharacters deducts a point for every character that is duplicated
at least twice.

If the duplicated character has instances side by side then an additional
point is deducted for each side by side instance
*/
type repeatCharacters struct {
	count  int
	lookup map[rune][]int
}

//newRepearCharacters creates a new instance
func newRepeatCharacters() *repeatCharacters {
	return &repeatCharacters{
		lookup: make(map[rune][]int),
	}
}

func (rc *repeatCharacters) consume(c rune, i, l int) {
	lc := c
	if isUpperCase(c) {
		lc = c + 32
	}

	if pos, ok := rc.lookup[lc]; ok {
		pos = append(pos, i)
		rc.lookup[lc] = pos
		if len(pos) == 2 {
			rc.count++
		}
		if pos[len(pos)-2]+1 == i {
			rc.count++
		}
	} else {
		rc.lookup[lc] = []int{i}
	}
}

func (rc *repeatCharacters) score(l int) int {
	return rc.count
}

/*
runningLower counts how my instances of lower case letters are side
by side. 2 points are deducted for every side by side instance
*/
type runningLower struct {
	last  bool
	count int
}

func (rl *runningLower) consume(c rune, i, l int) {
	res := isLowerCase(c)
	if rl.last && res {
		rl.count++
	}
	rl.last = res
}

func (rl *runningLower) score(l int) int {
	return rl.count * 2
}

/*
runningNumbers counts how many instances of digits are side by side.
2 points are deducted for every side by side instance.
*/
type runningNumbers struct {
	last  bool
	count int
}

func (rn *runningNumbers) consume(c rune, i, l int) {
	res := isDigit(c)
	if rn.last && res {
		rn.count++
	}
	rn.last = res
}

func (rn *runningNumbers) score(l int) int {
	return rn.count * 2
}

/*
runningUpper counts how many instances of upper case letters are side
by side. 2 points are deducted for every side by side instance.
*/
type runningUpper struct {
	last  bool
	count int
}

func (ru *runningUpper) consume(c rune, i, l int) {
	res := isUpperCase(c)
	if ru.last && res {
		ru.count++
	}
	ru.last = res
}

func (ru *runningUpper) score(l int) int {
	return ru.count * 2
}

/*
sequentialChars counts instances of letters or numbers ascending
or descending in at least 3 or more characters.

3 points are deducted for every character in the pattern starting
from the third character in the pattern.
*/
type sequentialChars struct {
	last     rune
	lastType string
	lastDir  string
	run      int
	count    int
}

func (sc *sequentialChars) consume(c rune, i, l int) {
	ctype := "s"
	if isLowerCase(c) || isUpperCase(c) {
		ctype = "c"
	} else if isDigit(c) {
		ctype = "d"
	}

	if sc.last > 0 && sc.lastType == ctype {
		if sc.last-c == 1 {
			if sc.lastDir == "d" {
				sc.run++
				if sc.run >= 2 {
					sc.count++
				}
			} else {
				sc.run = 1
			}
			sc.lastDir = "d"
		} else if c-sc.last == 1 {
			if sc.lastDir == "u" {
				sc.run++
				if sc.run >= 2 {
					sc.count++
				}
			} else {
				sc.run = 1
			}
			sc.lastDir = "u"
		} else {
			sc.run = 0
			sc.lastDir = "na"
		}
	} else {
		sc.lastDir = "na"
	}
	sc.last = c
	sc.lastType = ctype
}

func (sc *sequentialChars) score(l int) int {
	return sc.count * 3
}
