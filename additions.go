package pscore

/*
lowerCase returns 2 points for every lower case letter found
in a password.

If the password only contains lower case letters then 0
points are returned.
*/
type lowerCase struct {
	count int
}

func (lc *lowerCase) consume(c rune, i, l int) {
	if isLowerCase(c) {
		lc.count++
	}
}

func (lc *lowerCase) score(l int) int {
	if lc.count == 0 {
		return 0
	}
	return (l - lc.count) * 2
}

/*
middleNumSymbols assigns 2 points for every number or symbol
that is not the end or beginning of the password.
*/
type middleNumSymbols struct {
	count int
}

func (mns *middleNumSymbols) consume(c rune, i, l int) {
	if i > 0 && i < l-1 {
		if isDigit(c) || (!isLowerCase(c) && !isUpperCase(c)) {
			mns.count++
		}
	}
}

func (mns *middleNumSymbols) score(l int) int {
	return mns.count * 2
}

/*
numbers returns 4 points for every number found in a password.

If the password only consists of numbers then 0 points are returned.
*/
type numbers struct {
	count int
}

func (nu *numbers) consume(c rune, i, l int) {
	if isDigit(c) {
		nu.count++
	}
}

func (nu *numbers) score(l int) int {
	if nu.count == l {
		return 0
	}
	return nu.count * 4
}

/*
symbols returns 6 points for every symbol found in a password
*/
type symbols struct {
	count int
}

func (sy *symbols) consume(c rune, i, l int) {
	if isSymbol(c) {
		sy.count++
	}
}

func (sy *symbols) score(l int) int {
	return sy.count * 6
}

/*
upperCase returns 2 points for every upper case letter found in a
password.

If the password only consists of upper case letters then 0 points
are returned.
*/
type upperCase struct {
	count int
}

func (uc *upperCase) consume(c rune, i, l int) {
	if isUpperCase(c) {
		uc.count++
	}
}

func (uc *upperCase) score(l int) int {
	if uc.count == 0 {
		return 0
	}
	return (l - uc.count) * 2
}
