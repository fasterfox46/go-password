package pscore

import (
	"testing"
)

func TestSimplePassword(t *testing.T) {
	p := NewPassword("Test1234")
	score := p.GetScore()
	t.Log(p.GetScoreDetails())

	if score != 61 {
		t.Fatal("expected score", 61, ", got ", score)
	}
}

func TestLettersOnlyPassword(t *testing.T) {
	p := NewPassword("ThisIsATest")
	score := p.GetScore()
	t.Log(p.GetScoreDetails())
	if score != 42 {
		t.Fatal("expected", 42, "got", score)
	}
}

func TestComplexPassword(t *testing.T) {
	p := NewPassword("H3ll0-W0r1d!")
	score := p.GetScore()
	t.Log(p.GetScoreDetails())
	if score != 117 {
		t.Fatal("expected", 117, "got", score)
	}
}
