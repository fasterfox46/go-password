(function() {

  'use strict';

  var digitRgx = /\d/;
  var lowerRgx = /[a-z]/;
  var upperRgx = /[A-Z]/;
  var symbolRgx = /\W/;

  //isDigit determines if the character is a digit
  function isDigit(c) {
    return digitRgx.test(c);
  }

  //isLowerCase determines if the character is a lower case letter
  function isLowerCase(c) {
    return lowerRgx.test(c);
  }
  //isUpperCase determines if the character is an upper case letter
  function isUpperCase(c) {
    return upperRgx.test(c);
  }
  //isSymbol determines if the character is a symbol
  function isSymbol(c) {
    return symbolRgx.test(c);
  }

  /***** Additions *****/

  function LowerCase() {
    var count = 0;

    this.consume = consume;
    this.score = score;

    function consume(c) {
      if (isLowerCase(c)) {
        count++;
      }
    }

    function score(l) {
      if (count === 0)
        return 0;
      return (l - count) * 2;
    }
  }

  function MiddleNumSymbols() {
    var count = 0;

    this.consume = consume;
    this.score = score;

    function consume(c, i, l) {
      if (i > 0 && i < l - 1)
        if (isDigit(c) || (!isLowerCase(c) && !isUpperCase(c)))
          count++;
    }

    function score() {
      return count * 2;
    }
  }

  function Digits() {
    var count = 0;

    this.consume = consume;
    this.score = score;

    function consume(c) {
      if (isDigit(c))
        count++;
    }

    function score(l) {
      if (count === l)
        return 0;
      return count * 4;
    }
  }

  function Symbols() {
    var count = 0;

    this.consume = consume;
    this.score = score;

    function consume(c) {
      if (isSymbol(c))
        count++;
    }

    function score() {
      return count * 6;
    }
  }

  function UpperCase() {
    var count = 0;

    this.consume = consume;
    this.score = score;

    function consume(c) {
      if (isUpperCase(c))
        count++;
    }

    function score(l) {
      if (count === 0)
        return 0;
      return (l - count) * 2;
    }
  }


  /***** Deductions *****/

  function OnlyLetters() {
    var invalid = false;

    this.consume = consume;
    this.score = score;

    function consume(c) {
      if (!invalid && !isLowerCase(c) && !isUpperCase(c))
        invalid = true;
    }

    function score(l) {
      if (invalid)
        return 0;
      return l;
    }
  }

  function OnlyDigits() {
    var invalid = false;

    this.consume = consume;
    this.score = score;

    function consume(c) {
      if (!invalid && !isDigit(c))
        invalid = true;
    }

    function score(l) {
      if (invalid)
        return 0;
      return l;
    }
  }

  function RepeatCharacters() {
    var lookup = {},
      count = 0;

    this.consume = consume;
    this.score = score;

    function consume(c, i) {
      var lc = c.toLowerCase(),
        pos;
      if (typeof lookup[lc] !== 'undefined') {
        pos = lookup[lc];
        pos.push(i);

        if (pos.length === 2)
          count++;
        if (pos[pos.length - 2] + 1 === i)
          count++;
      } else {
        lookup[lc] = [i];
      }
    }

    function score() {
      return count;
    }
  }

  function RunningLower() {
    var last = false,
      count = 0;

    this.consume = consume;
    this.score = score;

    function consume(c) {
      var res = isLowerCase(c);
      if (res && last === res)
        count++;
      last = res;
    }

    function score() {
      return count * 2;
    }
  }

  function RunningDigits() {
    var last = false,
      count = 0;

    this.consume = consume;
    this.score = score;

    function consume(c) {
      var res = isDigit(c);
      if (res && last === res)
        count++;
      last = res;
    }

    function score() {
      return count * 2;
    }
  }

  function RunningUpper() {
    var last = false,
      count = 0;

    this.consume = consume;
    this.score = score;

    function consume(c) {
      var res = isUpperCase(c);
      if (res && last === res)
        count++;
      last = res;
    }

    function score() {
      return count * 2;
    }
  }

  function SequentialChars() {
    var count = 0,
      run = 0,
      lastDir, lastType, last;

    this.consume = consume;
    this.score = score;

    function consume(c) {
      var ctype = "s",
        code = c.charCodeAt();
      if (isLowerCase(c) || isUpperCase(c))
        ctype = "c";
      else if (isDigit(c))
        ctype = "d";

      if (last && lastType === ctype) {
        if (last - code === 1) {
          if (lastDir === 'd') {
            run++;
            if (run >= 2)
              count++;
          } else {
            run = 1;
          }
          lastDir = "d";
        } else if (code - last === 1) {
          if (lastDir === 'u') {
            run++;
            if (run >= 2)
              count++;
          } else {
            run = 1;
          }
          lastDir = "u";
        } else {
          run = 0;
          lastDir = "na";
        }
      } else {
        lastDir = "na";
      }
      last = code;
      lastType = ctype;
    }

    function score() {
      return count * 3;
    }
  }

  /***** General Functions ******/

  function PScore(password) {
    var word = password,
      l = word.length,
      score = l * 4,
      adds = {
        "Lower Case": new LowerCase(),
        "Middle Symbols & Numbers": new MiddleNumSymbols(),
        "Digits": new Digits(),
        "Symbols": new Symbols(),
        "Upper Case": new UpperCase()
      },
      deds = {
        "Only Letters": new OnlyLetters(),
        "Only Digits": new OnlyDigits(),
        "Repeating Characters": new RepeatCharacters(),
        "Consecutive Lower Case": new RunningLower(),
        "Consecutive Digits": new RunningDigits(),
        "Consecutive Upper Case": new RunningUpper(),
        "Sequential Characters": new SequentialChars()
      },
      i = 0,
      c, key;

    for (; i < l; i++) {
      c = word[i];
      for (key in adds) {
        adds[key].consume(c, i, l);
      }

      for (key in deds) {
        deds[key].consume(c, i, l);
      }
    }

    for (key in adds) {
      score += adds[key].score(l);
    }

    for (key in deds) {
      score -= deds[key].score(l);
    }

    this.score = function() {
      return score;
    };

    this.scoreDetails = function() {
      var output = {},
        key;
      output.Additions = {
        "Length": l * 4
      };
      for (key in adds) {
        output.Additions[key] = adds[key].score(l);
      }

      output.Deductions = {};
      for (key in deds) {
        output.Deductions[key] = deds[key].score(l);
      }
      output.Score = score;
      return output;
    };
  }

  var s = new PScore("H3ll0-W0r1d!");
  //var sc = s.score();
  s.scoreDetails();

})();
